import React, { Component } from 'react';
import io from 'socket.io-client';
import DataTable from '../components/DataTable';
import MonitoringQueue from '../components/MonitoringQueue';

class RealTimeStats extends Component {
    constructor() {
      super()
      this.state = {
        data: [],
        queues: [],
        onLoading: false
      }
      this.socket = io.connect('https://quintoandar.ngrok.io');
      this.socket.on('queue-go', (queues) => {
        this.setState({data: queues});
      });

      this.socket.on('worker-go', (workerUpdate) => {
        this.state.queues.forEach( (oldQueue) => {
          for (const [, newQueue] of workerUpdate.entries()) {
            if (oldQueue.open && oldQueue.sid === newQueue.sid) {
              newQueue.open = true;
            } else {
              continue;
            }
          }
      });
      this.setState({queues: workerUpdate});
      })
    }

    componentDidMount() {
      this.setState({
        onLoading: true
      });
      fetch('https://quintoandar.ngrok.io/queue')
        .then(res => res.json())
        .then(queues =>  {
          this.setState({data: queues, onLoading: false})
        })
        .catch(console.error)
    }

    columns = [
        {
          id: "friendlyName",
          type: "text",
          disablePadding: false,
          label: "Name",
          width: "10%",
        },
        {
          id: "averageWaitTime",
          type: "time-by-seconds",
          disablePadding: true,
          label: "Average wait time",
          width: "15%",
          highlight: true
        },
        {
          id: "totalAvailableWorkers",
          type: "number",
          disablePadding: true,
          label: "Available workers",
          width: "7%",
          highlight: true
        },
        {
          id: "totalEligibleWorkers",
          type: "number",
          disablePadding: true,
          label: "Eligible workers",
          width: "7%",
          highlight: true
        },
        {
          id: "tasksAssigned",
          type: "number",
          disablePadding: true,
          label: "Tasks assigned",
          width: "7%"
        },
        {
          id: "tasksCompleted",
          type: "number",
          disablePadding: true,
          label: "Tasks completed",
          width: "7%"
        },
        {
          id: "tasksCanceled",
          type: "number",
          disablePadding: true,
          label: "Tasks canceled",
          width: "7%"
        }
    ];

    handleClick(queue) {
      const filteredQueues = this.state.queues.filter(q => q.sid !== queue.sid);
      const withOpenQueue = [...filteredQueues, { ...queue, open: true }];
      this.setState({
        queues: withOpenQueue,
      })
    }

    focusQueue = (queue) => {
      this.setState({
        focused: queue.sid
      });
    }

    closeQueue = (queue) => {
      const filteredQueues = this.state.queues.filter(q => q.sid !== queue.sid);
      this.setState({ queues: filteredQueues });
    }

    validateDataLength = () => {
      if (this.state.onLoading) {
        return 'Carregando';
      } else if (!this.state.onLoading && !this.state.data.length) {
        return false
      }

    }

    render() {
      const {queues, data, focused} = this.state
      return (
        <>
          {queues.map((queue) => (
            queue.open &&
            <MonitoringQueue
              queue={queue}
              close={this.closeQueue}
              focus={this.focusQueue}
              focused={focused}
              key={queue.sid}
            />
          ))}
            <DataTable
                items={data}
                columns={this.columns}
                visible={true}
                disableOrdering
                onEmptyMessage={ this.validateDataLength()}
                onRowClick={this.handleClick.bind(this)}
            />
            </>
        )
    }
}

export default RealTimeStats;
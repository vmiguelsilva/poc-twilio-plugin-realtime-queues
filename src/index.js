import * as FlexPlugin from 'flex-plugin';
import NubankRealtimePlugin from './NubankRealtimePlugin';

FlexPlugin.loadPlugin(NubankRealtimePlugin);

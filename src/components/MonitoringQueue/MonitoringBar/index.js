import React from 'react';
import styled from 'styled-components';

const TitleBar = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  position: relative;`;

const Title = styled.span`
  width: 100%;
  text-align: center;
  color: #000;
  line-height: 30px;
  font-size: 14px;
  font-weight: 500;
`;

const WindowControll = styled.div`
  position: absolute;
  right: 0;
  top: 0;
  div {
    position: relative;
    float: left;
    width: 30px;
    height: 30px;
    &::before {
      top: 30%;
      right: 30%;
      bottom: 30%;
      left: 30%;
      content: " ";
      position: absolute;
      border-color: #CCCCCC;
      border-style: solid;
      border-width: 0 0 2px 0;
    }
    &::after  {
      top: 30%;
      right: 30%;
      bottom: 30%;
      left: 30%;
      content: " ";
      position: absolute;
      border-color: #CCCCCC;
      border-style: solid;
      border-width: 0 0 2px 0;
    }
    &::hover {
      background-color: #CCCCCC;
      cursor: pointer;
    }
  }
`;

const Minimize = styled.div`
  &::before {
    border-bottom-width: 2px;
  }
`;

const Maximize = styled.div`
  &::before {
    border-width: 1px !important;
  }
`;

const Close = styled.div`
  &::before {
    bottom: 50%!important;
    top: 50% !important;
    transform: rotate(45deg);
  }
  &::after {
    bottom: 50% !important;
    top: 50% !important;
    transform: rotate(-45deg);
  }
`;

const MonitoringBoxBar = (props) => {
  return (
    <TitleBar>
      <Title>
        {`${props.queue.friendlyName} - ${props.queue.taskOrder}`}
      </Title>
      <WindowControll >
        <Minimize onClick={props.onClickMinimize} />
        <Maximize  onClick={props.onClickMaximize} />
        <Close  onClick={props.onClickClose} />
      </WindowControll>
    </TitleBar>
  )
}

export default MonitoringBoxBar;
import React, { Component } from 'react';
import { Rnd } from 'react-rnd';
import DataTable from '../DataTable';
import MonitoringBoxBar from './MonitoringBar';


class MonitoringQueue extends Component {
    columns = [
        {
            id: 'name',
            type: 'text',
            disablePadding: true,
            label: 'Name',
            width: '25%',
            highlight: true
        }, {
            id: 'status',
            type: 'text',
            disablePadding: true,
            label: 'Status',
            width: '25%',
            highlight: false
        }, {
            id: 'dateStatusChanged',
            type: 'diff-time',
            disablePadding: true,
            label: 'Time in status',
            width: '25%',
            highlight: true
        }
    ]
    state = {
        box: {
            width: 664,
            height: 332,
            x: Math.random() * (window.innerWidth - 664),
            y: Math.random() * (window.innerHeight - 332),
        },
        lastMin: null,
        lastMax: null,
        maximized: false,
        minimized: false,
    }
    maximize = () => {
        if (this.state.minimized) return;
        if (this.state.maximized) {
            return this.setState({
                box: this.state.lastMax,
                minimized: false,
                maximized: false,
            });
        }

        const box = {
            width: window.innerWidth - 100,
            height: window.innerHeight,
            x: 10,
            y: 10,
        };
        return this.setState({
            box,
            lastMax: this.state.box,
            maximized: true,
        });
    }
    
    minimize = () => {
        if (this.state.minimized) {
            return this.setState({
            box: {
                ...this.state.box,
                height: this.state.lastMin.height,
            },
            minimized: false,
            });
        }

        const box = {
            ...this.state.box,
            height: 40
        };
        return this.setState({
            box,
            lastMin: this.state.box,
            minimized: true,
        })
    }
    render() {
        const { queue, focused, focus, close } = this.props;
        const rndStyle = {
            border: 'solid 1px #ddd',
            background: 'rgb(253, 253, 253)',
            minWidth: 400,
            overflowY: 'scroll',
            borderRadius: '5px',
            zIndex: focused === queue.sid ? 1301 : 1300
          };
        return (
            <Rnd
                style={rndStyle}
                size={{ width: this.state.box.width, height: this.state.box.height }}
                position={{ x: this.state.box.x, y: this.state.box.y }}
                onMouseDown={() => focus(queue)}
                onDrag={(e, d) => {
                this.setState({
                    box: {
                    ...this.state.box,
                    x: d.x,
                    y: d.y
                    }
                });
                }}
                onResize={(e, direction, ref, delta, position) => {
                this.setState({
                    box: {
                    width: ref.offsetWidth,
                    height: ref.offsetHeight,
                    ...position
                    }
                });
                }}
            >
                <MonitoringBoxBar
                    queue={queue}
                    onClickMinimize={this.minimize}
                    onClickMaximize={this.maximize}
                    onClickClose={(e) => {
                        e.stopPropagation();
                        close(queue);
                    }}
                    />
                <DataTable
                    columns={this.columns}
                    items={queue.workers}
                    visible={true}
                />
            </Rnd>
        );
    }
}

export default MonitoringQueue;
import React, { Component } from 'react';
import PropTypes from 'prop-types'
import './style.css';

class Checkbox extends Component {
  render() {
    return (
        <label className='control controlCheckbox'>
          <input type="checkbox" 
            checked={ this.props.checked } 
            value={ this.props.value } 
            onChange={ this.props.onCheck } />
          <div className='controlIndicator'></div>
        </label>
    );
  }
}

Checkbox.propTypes = {
  onCheck: PropTypes.func.isRequired,
  value: PropTypes.string,
  checked: PropTypes.bool
}

export default Checkbox;

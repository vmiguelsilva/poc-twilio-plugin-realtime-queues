import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  TableCell,
  TableHead,
  TableRow,
  Tooltip
} from '@material-ui/core';
import './style.css';
import Checkbox from '../../../Checkbox';

class DataTableHeader extends Component {
  state = {
    orderBy: {
      order: '',
      prop: ''
    }
  }

  sortHandler = (property) => event => {
    let { order, prop } = this.state;

    // resets the ordering because the user decided to order by a different column
    if (property !== prop) {
      prop = property;
      order = '';
    }

    // decides the next order for the current column
    switch (order) {
      case 'asc':
        prop = ''; // returns to the initial state in which no property is considered for ordering
        order = '';
        break;
      case 'desc':
        order = 'asc';
        break;
      default:
        order = 'desc';
    }

    this.setState({ order, prop });
    this.props.onRequestSort(order, prop);
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.columns.length !== nextProps.columns.length ||
      this.state.order !== nextState.order ||
      this.state.prop !== nextState.prop ||
      this.props.disableOrdering !== nextProps.disableOrdering ||
      this.props.checked !== nextProps.checked) {
      return true;
    }
    return false;
  }

  checkBoxColumn = () => {
    const { onCheck, checked } = this.props;
    return <Checkbox
      onCheck={onCheck}
      checked={checked}
    />;
  }

  orderableColumn = (column) => {
    
    const tooltipPlacement = column.type === 'numeric' ? 'bottom-end' : 'bottom-start';

    return <div>
      <Tooltip title={`Ordenar por ${column.label}`} placement={tooltipPlacement} className='tableCellOrderIcons'>
        <div onClick={this.sortHandler(column.id)}>
          {/* {tableHeaderOrderIcon} */}
        </div>
      </Tooltip>
      <div>
        {column.label}
      </div>
    </div>;
  }

  hiddenColumn = () => <div></div>

  normalColumn = (column) => <div>
    <div>
      {column.label}
    </div>
  </div>

  tableCell = (index, width, content) => {
    return <TableCell
        key={index}
        style={{ width }}>
        {content}
      </TableCell>
  }

  headers = () => {
    const { columns, enableActionColumn } = this.props;

    const cells = columns.map((column, index) => {
      let content;
      if (column.type === 'checkbox') {
        content = this.checkBoxColumn();
      } else if (column.isOrderable) {
        content = this.orderableColumn(column);
      } else {
        content = this.normalColumn(column);
      }

      return this.tableCell(index, column.width, content);
    });

    if (enableActionColumn) {
      cells.push(this.tableCell(null, null, this.hiddenColumn()));
    }

    return cells;
  }

  render() {
    return (
      <TableHead style={{
        padding: "unset",
        height: "34px"
      }}>
        <TableRow>
          <this.headers />
        </TableRow>
      </TableHead>
    );
  }
}

DataTableHeader.propTypes = {
  classes: PropTypes.object.isRequired,
  disableOrdering: PropTypes.bool,
  onRequestSort: PropTypes.func.isRequired,
  rowCount: PropTypes.number,
  onCheck: PropTypes.func,
  checked: PropTypes.bool,
  columns: PropTypes.array.isRequired,
  enableActionColumn: PropTypes.bool
};

export default DataTableHeader;

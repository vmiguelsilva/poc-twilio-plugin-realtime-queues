
import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import classnames from 'classnames';
import { TableCell, TableRow } from '@material-ui/core';
import Checkbox from '../../../Checkbox';
import './style.css';

function getStyle(props, item, index) {
  const { styleRows, styleRowsBy } = props;
  return styleRows[typeof styleRowsBy === 'function' ? styleRowsBy(item, index) : item[styleRowsBy]];
}

const DataTableRow = ({ ...props }) => {
  const {
    styleRows,
    styleRowsBy,
    columns,
    onRowClick,
    item,
    index,
    onCheck,
    checked,
    handleSelectedItem,
    actionIcon,
    hover
  } = props;

  const getValueFromStatus = () => {
      return item.statusPtBr
  };

  const countGroupByCellContent = (value) => {
    if (!value.length) {
      return <span className={'countGroupByCellEmpty'}>(não vinculado)</span>
    }

    return value.map((item, index) => {
      let label = item.values.length === 1 ? item.labelSingular : item.labelPlural;
      return <span className={'countGroupByCellText'}>{item.values.length} {label}</span>
    }).reduce((accu, curr, index) => {
      if (accu.length > 0) {
        accu.push(<span key={`${index}-sep`} className={classnames('countGroupByCellText', 'countGroupByCellBullet')}>&bull;</span>);
      }
      accu.push(curr);
      return accu;
    }, []);
  }

  const countGroupByCell = (key, className, value) => {
    return <TableCell
      key={key}
      className={className}>
      {countGroupByCellContent(value)}
    </TableCell>
  }

  const buildActionCell = (item) => {
    if (handleSelectedItem) {
      return <TableCell
        className={'trashBin'}
        onClick={() => handleSelectedItem(item)}>
        {actionIcon}
      </TableCell>
    }
  };

  const clickFn = onRowClick ? () => onRowClick(item, index) : null;
  let value = null;
  return (
    <TableRow className={`${(clickFn ? '' : 'rowCursorDefault')} 'row'`}
      style={styleRows && styleRowsBy ? getStyle(props, item, index) : null} hover={hover} tabIndex={-1} >
      {
        columns.map((column, i) => {
          const styleRule = column.id === 'statusPtBr' && column.getStyleRule ? column.getStyleRule(item[column.id], item.due_at) : '';
          const cellCursorPointer = column.onCellClick ? 'cellCursorPointer' : '';
          const cellCursorPointerCancel = column.onCellClick && item.statusPtBr !== 'Cancelada' ? 'cellCursorPointer' : '';
          const highlight = column.highlight ? 'highlight' : '';
          const clickHandler = onRowClick ? () => onRowClick(item, index) : column.onCellClick ? () => column.onCellClick(item, index) : f => f;
          switch (column.type) {
            case 'checkbox':
              return (
                <TableCell
                  key={i} className={`${'cell'} ${highlight}`}>
                  <Checkbox
                    value={item.number}
                    onCheck={onCheck}
                    checked={checked}
                  />
                </TableCell>
              );
            case 'number':
              value = !item[column.id.toString()] && typeof item[column.id] !== 'number' ? column.default : item[column.id.toString()];
              return (
                <TableCell
                  key={i}
                  onClick={clickHandler}
                  className={`${'cell'} ${cellCursorPointer} ${highlight}`}>
                  {value}
                </TableCell>
              );
            case 'date':
              value = item[column.id] && typeof item[column.id] !== 'object' ? moment(item[column.id]).format('DD/MM/YYYY') || column.default : '-';
              return (
                <TableCell
                  key={i}
                  onClick={clickHandler}
                  className={`${'cell'} ${styleRule} ${cellCursorPointerCancel} ${highlight}`}
                  component="td"
                  scope="row">
                  {value}
                </TableCell>
              );
            case 'time':
              value = moment(item[column.id]).format('HH:mm:ss') || column.default;
              return (
                <TableCell
                  key={i}
                  onClick={clickHandler}
                  className={`${'cell'} ${cellCursorPointer} ${highlight}`}
                  component="td"
                  scope="row">
                  {value}
                </TableCell>
              );
            case 'datetime':
              value = moment(item[column.id]).format('DD/MM/YYYY HH:mm:ss') || column.default;
              return (
                <TableCell
                  key={i}
                  onClick={clickHandler}
                  className={`${'cell'} ${cellCursorPointer} ${highlight}`}
                  component="td"
                  scope="row">
                  {value}
                </TableCell>
              );
            case 'price':
              value = `R$ ${parseFloat(item[column.id]).toFixed(2).replace('.', ',')}`;
              return (
                <TableCell
                  key={i}
                  onClick={clickHandler}
                  className={`${'cell'} ${cellCursorPointer} ${highlight}`}
                  component="td"
                  scope="row">
                  {value}

                </TableCell>
              );
            case 'diff-time': 
              const duration = moment.duration(moment().diff(moment(item[column.id])));
              return (
                <TableCell
                  key={i}
                  onClick={clickHandler}
                  className={`${'cell'} ${cellCursorPointer} ${highlight}`}
                  component="td"
                  scope="row">
                  {duration.humanize(true)}
                </TableCell>
            )
            case 'time-by-seconds':
              const time = moment().startOf('day').seconds(item[column.id]).format('H:mm:ss');
              return (
                <TableCell
                  key={i}
                  onClick={clickHandler}
                  className={`${'cell'} ${cellCursorPointer} ${highlight}`}
                  component="td"
                  scope="row">
                  {time}
                </TableCell>
            )
            case 'count-group-by':
              return countGroupByCell(i, classnames('cell', highlight), item[column.id]);
            default:
              value = !item[column.id] && typeof item[column.id] !== 'number' ? column.default : item[column.id];
              return (
                <TableCell
                  key={i}
                  onClick={clickHandler}
                  className={`${'cell'} ${styleRule} ${cellCursorPointerCancel} ${highlight}`}
                  component="td"
                  scope="row">
                  {value && (column.id === 'statusPtBr' ?
                    getValueFromStatus() : value.length > 35 ? value.slice(0, 35) + '...' : value)}
                </TableCell>
              );
          }
        })
      }
      {buildActionCell(item)}
    </TableRow>
  )
}

DataTableRow.propTypes = {
  columns: PropTypes.array.isRequired,
  item: PropTypes.object.isRequired,
  styleRows: PropTypes.object,
  styleRowsBy: PropTypes.any,
  onCheck: PropTypes.func,
  checked: PropTypes.bool,
  onRowClick: PropTypes.func,
  handleSelectedItem: PropTypes.func,
  actionIcon: PropTypes.node,
  createActionCell: PropTypes.func,
  hover: PropTypes.bool
};

DataTableRow.defaultProps = {
  hover: true
}

export default DataTableRow;

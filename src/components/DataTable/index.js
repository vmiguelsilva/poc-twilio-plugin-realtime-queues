import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import DataTableHeader from './components/DataTableHeader';
import DataTableRow from './components/DataTableRow';
import {
  Table,
  TableBody,
  TableCell,
  TableRow,
} from '@material-ui/core';
import './style.css';

class DataTable extends Component {
  orderBy = { order: '', prop: '' };
  state = {
    checked: {},
    allChecked: false
  }

  toggleAllChecked = () => {
    this.setState({ allChecked: !this.state.allChecked });
  }

  handleRequestSort = (order, prop) => {
    this.orderBy = { order, prop };
  };

  handleChangePage = (page) => {
    this.paginate.page = page;
  }

  handleChangeRowsPerPage = (value) => {
    this.paginate.limit = value;
  }

  findNumber(number) {
    return this.state.checked.findIndex((val) => val === number);
  }

  emptyRows = () => (this.props.items && this.props.items.length === 0) && (
    <TableRow>
      <TableCell colSpan={this.props.columns.length} style={{ textAlign: 'center' }}>
        {this.props.onEmptyMessage || 'Não existem dados'}
      </TableCell>
    </TableRow>
  );

  items = () => {
    const { items, onRowClick, columns, styleRows, styleRowsBy, handleSelectedItem, actionIcon, hover, customRowRender } = this.props;

    return items.map((item, i) => {
      if (customRowRender) {
        return React.cloneElement(customRowRender, {
          item,
          key: i,
          index: i,
        });
      }

      return (
        <DataTableRow
          key={i}
          item={item}
          columns={columns}
          index={i}
          onRowClick={onRowClick}
          styleRows={styleRows}
          styleRowsBy={styleRowsBy}
          handleSelectedItem={handleSelectedItem}
          actionIcon={actionIcon}
          hover={hover}
        />
      );
    });
  }

  render() {
    const { columns, items, hideHeaderOnEmpty, disableOrdering, actionIcon } = this.props;
    const { orderBy } = this;
    const hideUtils = hideHeaderOnEmpty && !items.length;

    return (
      <Fragment>
        <div>
          <div className='tableWrapper'>
            <Table className={this.props.tableStyles}>
              {
                !hideUtils &&
                <DataTableHeader
                  columns={columns}
                  disableOrdering={disableOrdering}
                  order={orderBy && orderBy.order}
                  orderProp={orderBy && orderBy.prop}
                  onRequestSort={this.handleRequestSort}
                  rowCount={items.length}
                  checked={this.state.allChecked}
                  enableActionColumn={!!actionIcon}
                />
              }
              <TableBody>
                <this.items />
                <this.emptyRows />
              </TableBody>
            </Table>
          </div>
        </div>
      </Fragment>
    );
  }
}

DataTable.propTypes = {
  columns: PropTypes.array.isRequired,
  disablePagination: PropTypes.bool,
  disableOrdering: PropTypes.bool,
  items: PropTypes.array.isRequired,
  hideHeaderOnEmpty: PropTypes.bool,
  onEmptyMessage: PropTypes.string,
  orderBy: PropTypes.object,
  paginate: PropTypes.object,
  visible: PropTypes.bool,
  styleRows: PropTypes.object,
  dataTableBar: PropTypes.bool,
  styleRowsBy: PropTypes.func,
  onRowClick: PropTypes.func,
  onSearch: PropTypes.func,
  onDeleteClick: PropTypes.func,
  onCheckItem: PropTypes.func,
  onUncheckItem: PropTypes.func,
  checkedItems: PropTypes.bool,
  checkAllItems: PropTypes.func,
  uncheckAllItems: PropTypes.func,
  actionIcon: PropTypes.node,
  tableStyles: PropTypes.string,
  hover: PropTypes.bool,
  customRowRender: PropTypes.element,
  handleSelectedItem: PropTypes.func,
  createActionCell: PropTypes.func
};

export default DataTable;
